package org.firstinspires.ftc.teamcode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;

import org.firstinspires.ftc.robotcontroller.external.samples.ConceptVuforiaFieldNavigationWebcam;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
public class lilBot {

    public enum Direction {
        FORWARD,
        BACKWARD,
        LEFT,
        RIGHT,
        SPIN_LEFT,
        SPIN_RIGHT
    }

    private DcMotor backLeft;
    private DcMotor backRight;
    private DcMotor frontLeft;
    private DcMotor frontRight;
    private CRServo pushy;
    private DcMotor elevator;
    private DcMotor duckSpinny;

    WebcamName webCam;
    //backLeft
    //backRight
    //frontLeft
    //frontRight
    private ConceptVuforiaFieldNavigationWebcam phone;

    private int encod = -1;

    public void setEncoderTargets(int fL, int fR, int bL, int bR, double speed, int distance) {
        frontLeft.setTargetPosition(fL * distance * encod);
        frontRight.setTargetPosition(fR * distance * encod);
        frontLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        frontRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        frontRight.setPower(speed);
        frontLeft.setPower(speed);


        backLeft.setTargetPosition(bL * distance * encod);
        backRight.setTargetPosition(bR * distance * encod);
        backLeft.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        backRight.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        backRight.setPower(speed);
        backLeft.setPower(speed);
    }
    //Drive using encoders
    public void goInDirection(Direction direction, int distance, double speed) {

        switch(direction) {
            case FORWARD:
                //drive(1,1,1,1,speed)
                setEncoderTargets(1,1,1,1,speed,distance);
                break;
            case BACKWARD:
               setEncoderTargets(-1,-1,-1,-1,speed,distance);
                break;
            case LEFT:
                setEncoderTargets(-1,1,1,-1,speed,distance);
                break;
            case RIGHT:
                setEncoderTargets(1,-1,-1,1,speed,distance);
                break;
            case SPIN_RIGHT:
                setEncoderTargets(-1,1,-1,1,speed,distance);
                break;
            case SPIN_LEFT:
                setEncoderTargets(1,-1,1,-1,speed,distance);
                break;
        }
    }

    public void init(HardwareMap map) {
        backLeft=map.dcMotor.get("backLeft");
        backRight=map.dcMotor.get("backRight");
        frontLeft=map.dcMotor.get("frontLeft");
        frontRight=map.dcMotor.get("frontRight");
        pushy=map.crservo.get("pushy");

        backRight.setDirection(DcMotorSimple.Direction.REVERSE);
        frontRight.setDirection(DcMotorSimple.Direction.REVERSE);

        phone=new ConceptVuforiaFieldNavigationWebcam();

    }

    public DcMotor getBackLeft() {
        return backLeft;
    }
    public DcMotor getBackRight() {
        return backRight;
    }
    public DcMotor getFrontLeft() {
        return frontLeft;
    }
    public DcMotor getFrontRight() {
        return frontRight;
    }

    public CRServo getPushy() {
        return pushy;
    }

    public WebcamName getWebCam() {
        return webCam;
    }

    public void stop() {
        backLeft.setPower(0);
        backRight.setPower(0);
        frontLeft.setPower(0);
        frontRight.setPower(0);
    }

    //Make encoders work

    public void setEncoders(boolean enabled) {
        if (enabled) {
            frontRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            frontLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            backRight.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            backLeft.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        } else {
            frontRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            frontLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            backRight.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
            backLeft.setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        }
    }

    //Time for vroomies

    public void drive(double fL, double fR, double bL, double bR, double speed) {
        frontLeft.setPower(fL*speed);
        frontRight.setPower(fR*speed);
        backLeft.setPower(bL*speed);
        backRight.setPower(bR*speed);
    }

    public void forward(double speed) { drive(1,1,1,1,speed); }

    public void backward(double speed) { drive(-1,-1,-1,-1,speed); }

    public void spinleft(double speed) { drive(1,-1,1,-1,speed); }

    public void spinright(double speed) { drive(-1, 1, -1,1,speed); }

    //IF WE CAN MOVE LEFT AND RIGHT
    public void left(double speed){ drive(-1, 1, 1, -1, speed); }

    public void right(double speed){ drive(1, -1, -1, 1, speed); }

    /*IF WE CAN MOVE DIAGONALLY
    public void forwardRight(double speed){ drive(1,0,0,1,speed); }

    public void forwardLeft(double speed){ drive(0,1,1,0,speed); }

    public void backwardRight(double speed){ drive(0,-1,-1,0,speed); }

    public void backwardLeft(double speed){ drive(-1,0,0,-1,speed); }*/
}
