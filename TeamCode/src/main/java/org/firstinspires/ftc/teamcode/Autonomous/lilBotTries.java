package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.lilBot;

@Autonomous (name="lilBotTries")
public class lilBotTries extends OpMode {
    ElapsedTime timer;
    private lilBot robot;
    private int state;

    public void init() {
        state=0;

        robot=new lilBot();
        robot.init(hardwareMap);
        robot.getBackLeft().setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.getBackRight().setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.getFrontLeft().setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        robot.getFrontRight().setMode(DcMotor.RunMode.RUN_WITHOUT_ENCODER);
        timer = new ElapsedTime();
        timer.reset();
    }
    public void loop() {
        switch(state){

            case 0:
               next();

                break;
            case 1:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("timer", timer.seconds());
                robot.forward(0.8);
                 if (timer.seconds() > 2) {
                     next();
                 }
                 break;
            case 2:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("timer", timer.seconds());
                break;
        }
    }

    private void next() {
        state++;
        timer.reset();
        robot.stop();
    }
}
