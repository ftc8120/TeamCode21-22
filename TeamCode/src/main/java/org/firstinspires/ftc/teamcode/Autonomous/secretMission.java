package org.firstinspires.ftc.teamcode.Autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.teamcode.ChadBot;

import java.util.List;

@Autonomous(name = "secretMission")
public class secretMission extends OpMode {

    ElapsedTime timer;
    private ChadBot robot;
    private double speed;
    private int state;
    private int inttterState;
    WebcamName webcamName;
    OpenGLMatrix lastLocatiofn = null;
    String targetName = "";
    final double DESIRED_DISTANCE = 8.0; //  this is how close the camera should get to the target (inches)
    final double SPEED_GAIN =   0.02 ;   //  The GAIN constants set the relationship between the measured position error,
    final double TURN_GAIN  =   0.01 ;   //  and how much power is applied to the drive motors.  Drive = Error * Gain
                                        //  Make these values smaller for smoother control.
                                        //  Speed Control "Gain". eg: Ramp up to 50% power at a 25 inch error.   (0.50 / 25.0)
                                       //  Turn Control "Gain".  eg: Ramp up to 25% power at a 25 degree error. (0.25 / 25.0)

    final double MM_PER_INCH = 25.40 ;

    private static final String VUFORIA_KEY =
            "AYef6RP/////AAABmQhqgETT3Uq8mNFqAbjPOD990o1n/Osn3oBdTsKI0NXgPuXS612xYfN5Q65srnoMx2" +
                    "eKXe32WnMf6M2BSJSgoPfTZmkmujVujpE/hUrmy5p4L7CALtVoM+TDkfshpKd+LGJT834pEOYU" +
                    "qcUj+vySs3OZQNepaSflmiShfHRNVbrgjrEs1Erlg7zZzc6EQo+yvh0fFtUiQUPLCCcZEPyfnU" +
                    "4k0o8phhbR+Ca9B6dtoeNaYITGHvMmOkBLsyAnR/RQ4Xv8KpvSaSfk0PDyzCG7UsN49k055xOx" +
                    "kFI0iKYp7NMCDF+cezE80dkcnpZCzg1RpGuSpCKGuUbSkJp+q5qudl2qZfWnQntaNI0vlNKD2x1C";

    /**
     * {@link #vuforia} is the variable we will use to store our instance of the Vuforia
     * localization engine.
     */
    private static final String TFOD_MODEL_ASSET = "FreightFrenzy_BCDM.tflite";
    private static final String[] LABELS = {
            "Ball",
            "Cube",
            "Duck",
            "Marker",
    };
    private VuforiaLocalizer vuforia;

    /**
     * {@link #tfod} is the variable we will use to store our instance of the TensorFlow Object
     * Detection engine.
     */
    private TFObjectDetector tfod;
    public static final String TAG = "Vuforia VuMark Sample";

    private void initVuforia() {
        /*
         * Configure Vuforia by creating a Parameter object, and passing it to the Vuforia engine.
         */
        //VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();

        //  Instantiate the Vuforia engine
        webcamName=robot.getwebcamName();
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);
        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.useExtendedTracking = false;
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        parameters.cameraName = webcamName;
        vuforia = ClassFactory.getInstance().createVuforia(parameters);
        // Loading trackables is not necessary for the TensorFlow Object Detection engine.
        VuforiaTrackables targetsFreightFrenzy = this.vuforia.loadTrackablesFromAsset("FreightFrenzy");
        targetsFreightFrenzy.get(0).setName("Blue Storage");
        targetsFreightFrenzy.get(1).setName("Blue Alliance Wall");
        targetsFreightFrenzy.get(2).setName("Red Storage");
        targetsFreightFrenzy.get(3).setName("Red Alliance Wall");
        targetsFreightFrenzy.activate();

        //give me spliterator nowwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww
        //return spliterator(pls);
    }

    /**
     * Initialize the TensorFlow Object Detection engine.
     */
    private void initTfod() {
        int tfodMonitorViewId = hardwareMap.appContext.getResources().getIdentifier(
                "tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minResultConfidence = 0.5f;
        tfodParameters.isModelTensorFlow2 = true;
        tfodParameters.inputSize = 320;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABELS);
    }

    @Override
    public void init() {
        initVuforia();
        initTfod();
        if (tfod != null) {
            tfod.activate();
            tfod.setZoom(1.25, 8.0 / 4.5);//change
            //can change mag later to find seomthing better- if want to test
            //dont change ratio
        }


        timer = new ElapsedTime();
        robot = new ChadBot();
        robot.init(hardwareMap);
        speed = .5;
        state = 0;

        robot.getBackLeft().setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.getBackRight().setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);



    }

    public void next() {
        state++;
        timer.reset();
        robot.stop();
        robot.getBackLeft().setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        robot.getBackRight().setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
    }

    @Override
    public void loop() {
        switch (state) {

            case 0:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("backRight",robot.getBackRight().getCurrentPosition());
                telemetry.addData("backLeft",robot.getBackLeft().getCurrentPosition());
                //robot.forward(1);
                robot.encoder(250,250, .15);
                if (!robot.getBackLeft().isBusy() && !robot.getBackRight().isBusy()) {
                    next();
                }
                break;

            case 1:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("backRight",robot.getBackRight().getCurrentPosition());
                telemetry.addData("backLeft",robot.getBackLeft().getCurrentPosition());
               // if (!robot.getBackLeft().isBusy() && !robot.getBackRight().isBusy()) {
                    next();
                //}
                break;

            case 2:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("backRight",robot.getBackRight().getCurrentPosition());
                telemetry.addData("backLeft",robot.getBackLeft().getCurrentPosition());

                //robot.encoder(-670,650,.25);
                robot.clockwiseDuckyTurn();
                if (timer.seconds() > 4)
                    next();
                break;

            case 3:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("backRight",robot.getBackRight().getCurrentPosition());
                telemetry.addData("backLeft",robot.getBackLeft().getCurrentPosition());

                robot.encoder(-100,100, .25);
                if (!robot.getBackLeft().isBusy() && !robot.getBackRight().isBusy()) {
                    next();
                }
                break;

            case 4:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("backRight",robot.getBackRight().getCurrentPosition());
                telemetry.addData("backLeft",robot.getBackLeft().getCurrentPosition());
              //  robot.encoder(1000,1000);
                //robot.counterClockwiseDuckyTurn();
                robot.encoder(-4000,-4000,.3);
                if (timer.seconds() > 5) {
                    next();
                }
                break;
/*
            case 4:
                telemetry.addData(String.format("State (%d)", state), state);
                telemetry.addData("backRight",robot.getBackRight().getCurrentPosition());
                telemetry.addData("backLeft",robot.getBackLeft().getCurrentPosition());

                robot.encoder(-10000,-10000, .25);
                if (!robot.getBackLeft().isBusy() && !robot.getBackRight().isBusy()) {
                    next();
                }
                break;*/
        }
    }
}