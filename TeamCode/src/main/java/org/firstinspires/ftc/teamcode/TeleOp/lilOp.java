package org.firstinspires.ftc.teamcode.TeleOp;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.lilBot;

@TeleOp(name="lilOp")
public class lilOp extends OpMode {
    private lilBot robot;
    public void init() {
        robot=new lilBot();
        robot.init(hardwareMap);
    }
    public void loop() {
        updateDriver();

        telemetry.addData(String.format("State (%.5f)", gamepad1.left_stick_y), gamepad1.left_stick_y);
        telemetry.addData(String.format("State (%.5f)", gamepad1.left_stick_x), gamepad1.left_stick_x);
        telemetry.addData(String.format("State (%.5f)", gamepad1.right_stick_x), gamepad1.right_stick_x);
        telemetry.addData(String.format("State (%.5f)", gamepad1.right_stick_y), gamepad1.right_stick_y);
        telemetry.update();
    }

    private void updateDriver() { //Just Driving
        if(Math.abs(gamepad1.left_stick_y)>0.1) {
            if(gamepad1.left_stick_y>0) {
                robot.backward(gamepad1.left_stick_y);
            }
            else {
                robot.forward(-gamepad1.left_stick_y);
            }
        }
        if(Math.abs(gamepad1.left_stick_x)>0.1) {
            if(gamepad1.left_stick_x>0) {
                robot.right(gamepad1.left_stick_x);
            }
            else {
                robot.left(-gamepad1.left_stick_x);
            }
        }
        if(Math.abs(gamepad1.right_stick_x)>0.1) {
            if(gamepad1.right_stick_x>0) {
                robot.spinright(gamepad1.right_stick_x);
            }
            else {
                robot.spinleft(-gamepad1.right_stick_x);
            }
        }

        if(Math.abs(gamepad1.left_stick_y)<=.1&&Math.abs(gamepad1.right_stick_x)<=.1&&Math.abs(gamepad1.left_stick_x)<=.1){
            robot.stop();
        }

    }


}
