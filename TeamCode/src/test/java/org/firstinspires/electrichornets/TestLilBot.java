package org.firstinspires.electrichornets;

import static org.junit.Assert.assertEquals;

import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.lilBot;
import org.junit.Test;

public class TestLilBot {
    @Test
    public void hi() {

        lilBot charles = new lilBot();
        HardwareMap map = new HardwareMap(null);
        FakeMotor backLeft = new FakeMotor();
        map.dcMotor.put("backLeft", backLeft);
        map.dcMotor.put("backRight", new FakeMotor());
        map.dcMotor.put("frontLeft", new FakeMotor());
        map.dcMotor.put("frontRight", new FakeMotor());
        map.crservo.put("pushy", new FakeCRServo());
        charles.init(map);
        charles.setEncoders(true);
        charles.setEncoderTargets(1,1,1,1,1,1);
        assertEquals(1,backLeft.getPower(),0.0000000000001);
    }
}
